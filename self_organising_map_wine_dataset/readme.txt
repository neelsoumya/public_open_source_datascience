An implementation of a self-organsing map using the data science tool Orange.

The data is the Wine dataset from the UCI machine learning repository.

http://orange.biolab.si/widget-catalog/unsupervised/selforganizingmap/